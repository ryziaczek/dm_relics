#  -*- coding: utf8 -*-
import json
import os.path
import codecs

import mysql.connector

my_user = 'root'
my_password = 'root'
my_host = '127.0.0.1'
json_path = "D:\\EksplotacjaDanych\\Kuba\\relics-json\\"


class Parser_json:
    @staticmethod
    def getAllProperties():
        numberOfJson = getNumberOfJsons(json_path)
        namesOfJsons = getNamesOfJsons(json_path)

        _json = {}
        maxValueofKeys = 0
        properties = {}

        for i in range(numberOfJson - 1):
            _json = decodeJsonByName(namesOfJsons[i])
            properties.update(_json)

            # Zmienna dlugosc od 19 do 14 zauwazylem nic innego sie nie pojawialo
            if len(_json) > maxValueofKeys :
                 print( len(_json))
                 maxValueofKeys = len(_json)
            elif len(_json) < maxValueofKeys:
                 print(len(_json))

            #Progress indicator
            if i % 1000 == 0:
                print("progress: " + str(i))

        print("max value of keys: " + str(maxValueofKeys))
        return properties.keys()



def create_database(name):
    cnx = mysql.connector.connect(user=my_user, password=my_password, host=my_host)
    cursor = cnx.cursor()
    try:
        cursor.execute("DROP DATABASE IF EXISTS " + name)
        cursor.execute(
            "CREATE DATABASE {} DEFAULT CHARACTER SET 'utf8'".format(name))
        print "CREATE DATABASE " + name
    except mysql.connector.Error as err:
        print("Failed creating database: {}".format(err))
        exit(1)

    cursor.close()
    cnx.close()


# show tables from db;
# select from * table;
def create_tables(cursor):

    #VOIVODESHIP
    cursor.execute("DROP DATABASE IF EXISTS voivodeship")
    cursor.execute("CREATE  TABLE IF NOT EXISTS voivodeship ("
    "id INT UNSIGNED NOT NULL AUTO_INCREMENT ,"
    "voivodeship_name VARCHAR(255) NOT NULL ,"
    "PRIMARY KEY (id),"
    "UNIQUE INDEX name_UNIQUE (voivodeship_name ASC) )"
    )

    #District
    cursor.execute("DROP TABLE IF EXISTS district")
    cursor.execute("CREATE TABLE IF NOT EXISTS district ("
    "id INT UNSIGNED NOT NULL AUTO_INCREMENT ,"
    "voivodeship_id INT UNSIGNED NOT NULL ,"
    "district_name VARCHAR(255) NOT NULL ,"
    "PRIMARY KEY (id) ,"
    "UNIQUE INDEX name_UNIQUE (district_name ASC) ,"
    "FOREIGN KEY (voivodeship_id) REFERENCES voivodeship (id) )"
    )

    #commune
    cursor.execute("DROP TABLE IF EXISTS commune")
    cursor.execute("CREATE TABLE IF NOT EXISTS commune ("
    "id INT UNSIGNED NOT NULL AUTO_INCREMENT ,"
    "district_id INT UNSIGNED NOT NULL ,"
    "commune_name VARCHAR(255) NOT NULL ,"
    "PRIMARY KEY (id) ,"
    "UNIQUE INDEX name_UNIQUE (commune_name ASC) ,"
    "FOREIGN KEY (district_id) REFERENCES district (id) )"
    )

    #place
    cursor.execute("DROP TABLE IF EXISTS place")
    cursor.execute("CREATE TABLE IF NOT EXISTS place ("
    "id INT UNSIGNED NOT NULL ,"
    "commune_id INT UNSIGNED NOT NULL ,"
    "place_name VARCHAR(255) NOT NULL ,"
    "PRIMARY KEY (id) ,"
    "UNIQUE INDEX name_UNIQUE (place_name ASC) ,"
    "FOREIGN KEY (commune_id) REFERENCES commune (id) )"
    )

    #relic
    cursor.execute("DROP TABLE IF EXISTS relic")
    cursor.execute("CREATE TABLE IF NOT EXISTS relic ("
    "id INT UNSIGNED NOT NULL ,"
    "nid_id INT UNSIGNED NULL ,"
    "identification VARCHAR(255) NULL,"
    "place_id INT UNSIGNED NULL ,"
    "common_name VARCHAR(255) NULL ,"
    "description TEXT NULL ,"
    "state ENUM('unchecked','checked','filled') NOT NULL ,"

    "PRIMARY KEY (id) ,"
    "FOREIGN KEY (place_id) REFERENCES place (id) )"

    )


    #events
    cursor.execute("DROP TABLE IF EXISTS events")
    cursor.execute("CREATE TABLE IF NOT EXISTS events ("
    "id INT UNSIGNED NOT NULL ,"
    "name VARCHAR(255) NOT NULL ,"
    "date VARCHAR(255) NOT NULL ,"
    "relic_id INT UNSIGNED NOT NULL,"
    "PRIMARY KEY (id),"
    "FOREIGN KEY (relic_id )REFERENCES relic (id ))")


    #entries
    cursor.execute("DROP TABLE IF EXISTS entries")
    cursor.execute("CREATE TABLE IF NOT EXISTS entries ("
    "id INT UNSIGNED NOT NULL ,"
    "title VARCHAR(255) NOT NULL ,"
    "body TEXT NOT NULL ,"
    "relic_id INT UNSIGNED NOT NULL ,"
    "PRIMARY KEY (id) ,"
    "FOREIGN KEY (relic_id ) REFERENCES relic (id) )")

    #documents
    cursor.execute("DROP TABLE IF EXISTS documents")
    cursor.execute("CREATE TABLE IF NOT EXISTS documents ("
    "id INT UNSIGNED NOT NULL ,"
    "name VARCHAR(255) NULL ,"
    "description TEXT NULL ,"
    "url VARCHAR(255) NOT NULL ,"
    "relic_id INT UNSIGNED NOT NULL ,"
    "PRIMARY KEY (id) ,"
    "FOREIGN KEY (relic_id ) REFERENCES relic (id) )")


    #alerts
    cursor.execute("DROP TABLE IF EXISTS alerts")
    cursor.execute("CREATE TABLE IF NOT EXISTS alerts ("
    "id INT UNSIGNED NOT NULL ,"
    "url VARCHAR(255) NULL ,"
    "author VARCHAR(255) NULL ,"
    "date_taken VARCHAR(255) NULL ,"
    "description TEXT NOT NULL ,"
    "state ENUM('new','in_progress','fixed') NOT NULL ,"
    "relic_id INT UNSIGNED NOT NULL ,"
    "PRIMARY KEY (id) ,"
    "FOREIGN KEY (relic_id ) REFERENCES relic (id) )")


    #links
  #   cursor.execute("DROP TABLE IF EXISTS alerts")
  #   cursor.execute("CREATE TABLE IF NOT EXISTS alerts ("
  #    `id` INT UNSIGNED NOT NULL ,
  # `monument_id` INT UNSIGNED NOT NULL ,
  # `category_id` INT UNSIGNED NOT NULL ,
  # `kind` ENUM('paper','url') NOT NULL ,
  # `name` VARCHAR(255) NOT NULL ,
  # `url` VARCHAR(255) NULL ,
  # PRIMARY KEY (`id`) ,
def insert_into_tables(cursor):
    numberOfJsons = getNumberOfJsons(json_path)
    namesOfJsons = getNamesOfJsons(json_path)

    #numberOfJsons = 1000
    print(numberOfJsons)

    for i in range(numberOfJsons - 1):
        if i % 1000 == 0:
            print("progress: " + str(i))

        _json = decodeJsonByName(namesOfJsons[i])

        ### voivodeship ####
        cursor.execute("SELECT * FROM voivodeship where voivodeship_name = '"+_json['voivodeship_name']+"'")
        if len(cursor.fetchall()) == 0:
            cursor.execute(
                  "INSERT INTO voivodeship " +
                 "(voivodeship_name) "
                 "VALUES(%s )",
                 ( _json['voivodeship_name'],
                   ))

        ### district ####
        cursor.execute("SELECT * FROM district where district_name = '"+_json['district_name']+"'")
        if len(cursor.fetchall()) == 0:
            cursor.execute("SELECT id FROM voivodeship where voivodeship_name = '"+_json['voivodeship_name']+"'")
            id_voivodeship=cursor.fetchall()[0][0]

            cursor.execute(
                      "INSERT INTO district " +
                     "(district_name ,voivodeship_id) "
                     "VALUES(%s,%s )",
                     ( _json['district_name'],
                       id_voivodeship,
                       ))

        ### commune####
        cursor.execute("SELECT * FROM commune where commune_name = '"+_json['commune_name']+"'")
        if len(cursor.fetchall()) == 0:
            cursor.execute("SELECT id FROM district where district_name = '"+_json['district_name']+"'")
            id_district=cursor.fetchall()[0][0]

            cursor.execute(
                      "INSERT INTO commune " +
                     "(commune_name ,district_id) "
                     "VALUES(%s,%s )",
                     ( _json['commune_name'],
                       id_district,
                       ))

        ### place ####
        cursor.execute("SELECT * FROM place where place_name = '"+_json['place_name']+"'")
        if len(cursor.fetchall()) == 0:
            cursor.execute("SELECT id FROM commune where commune_name = '"+_json['commune_name']+"'")
            id_commune=cursor.fetchall()[0][0]

            cursor.execute(
                      "INSERT INTO place " +
                     "(place_name ,commune_id,id) "
                     "VALUES(%s,%s,%s )",
                     ( _json['place_name'],
                       id_commune,
                       _json['place_id'],
                       ))

        #relic
        cursor.execute("SELECT id FROM place where place_name = '"+_json['place_name']+"'")
        place_id=cursor.fetchall()[0][0]
        cursor.execute(
                      "INSERT INTO relic " +
                     "(id ,nid_id,identification,place_id,common_name,description ,state) "
                     "VALUES(%s,%s,%s,%s,%s,%s,%s)",
                     ( _json['id'],
                       _json['nid_id'],
                       _json['identification'],
                        place_id,
                       _json['common_name'],
                       _json['description'],
                       _json['state']

                       ))

        #events
        if _json['events']:
            #print _json['events'][0]['id'],_json['events'][0]['date'],_json['events'][0]['name']
            for event in _json['events']:
                cursor.execute(
                  "INSERT INTO events" +
                 "(id,name,date,relic_id) "
                 "VALUES(%s,%s,%s,%s )",
                 ( event['id'],
                  event['name'],
                  event['date'],
                  _json['id'],
                  ))

        #entries
        if _json['entries']:
            for entry in _json['entries']:
                cursor.execute(
                  "INSERT INTO entries" +
                 "(id,title,body,relic_id) "
                 "VALUES(%s,%s,%s,%s )",
                 ( entry['id'],
                  entry['title'],
                  entry['body'],
                  _json['id'],
                  ))

        #documents
        if _json['documents']:
            #print _json['documents']
            for document in _json['documents']:
                cursor.execute(
                  "INSERT INTO documents" +
                 "(id,name,description,url,relic_id) "
                 "VALUES(%s,%s,%s,%s,%s )",
                 ( document['id'],
                  document['name'],
                  document['description'],
                  document['url'],
                  _json['id'],
                  ))
        #aletrs
        if _json['alerts']:
            for alert in _json['alerts']:
                cursor.execute(
                  "INSERT INTO alerts" +
                 "(id,url,author,date_taken,description,state,relic_id) "
                 "VALUES(%s,%s,%s,%s,%s,%s,%s  )",
                 (alert['id'],
                  alert['url'],
                  alert['author'],
                  alert['date_taken'],
                  alert['description'],
                  alert['state'],
                  _json['id'],
                  ))
        #if _json['links']:
        #    print _json['links']


def get_table(cursor, table_name):
    print " --- "+ table_name+ " --- "
    cursor.execute("SELECT * FROM "+table_name)
    for row in cursor.fetchall():
        print row


def decodeJsonByName(name):
    if os.path.isfile(name):
        with codecs.open(name) as data_file:
            data = json.load(data_file)
            data_file.close()

            return data



def getNamesOfJsons(path):
    namesOfJsons = []
    for file in os.listdir(path):
        if file.endswith(".json"):
            namesOfJsons.append(path + file)
    return namesOfJsons


def getNumberOfJsons(path):
    return len([name for name in os.listdir(path) if os.path.isfile(os.path.join(path, name))])


###### Main ########
#print(Parser_json.getAllProperties())

db_name = "mama"
table_name = "monument"

create_database(db_name)
cnx = mysql.connector.connect(user=my_user, password=my_password, host=my_host, database=db_name)
cursor = cnx.cursor()


create_tables(cursor)
insert_into_tables(cursor)

#get_table(cursor, "voivodeship")
#get_table(cursor, "district")
#get_table(cursor, "commune")
#get_table(cursor,"place")
#get_table(cursor,"relic")
#get_table(cursor,"events")
#get_table(cursor,"documents")

cnx.commit()
cursor.close()
cnx.close()